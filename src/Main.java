public class Main {
    boolean[][] board = new boolean[8][8];
    int checks = 0;

    public static void main(String[] args) {
        new Main().solve();
    }

    boolean checkPosition(int row, int column) {
        checks++;

        // Check column until current row reached; there are no queans placed (yet) after the current row
        for(int checkRow = 0; checkRow < row; checkRow++) {
            if (board[checkRow][column])
                return false;
        }
        // Checking all columns of the row; is not necessary because only one queen is placed every column
//        for(int checkColumn = 0; checkColumn < 8; checkColumn++) {
//            if (board[row][checkColumn] && checkColumn != column)
//                return false;
//        }
        // diagonal checking, start at the next position after newly placed queen, never more than 7 steps to go
        for (int i = 1; i < 8; i++) {
            int checkRow = row + i;
            int checkColumn = column + i;
            // not over the board on the top?
            if (checkRow < 8) {
                // not over the right hand side and another queen found?
                if (checkColumn < 8 && board[checkRow][checkColumn]) {
                    return false;
                }
                // not over then left hand side and another queen found?
                checkColumn = column - i;
                if (checkColumn >= 0 && board[checkRow][checkColumn]) {
                    return false;
                }
            }
            checkRow = row - i;
            checkColumn = column - i;
            // not over the bottom?
            if (checkRow >= 0) {
                // not over then left hand side and another queen found?
                if (checkColumn >= 0 && board[checkRow][checkColumn]) {
                    return false;
                }
                checkColumn = column + i;
                // not over the right hand side and another queen found?
                if (checkColumn < 8 && board[checkRow][checkColumn]) {
                    return false;
                }
            }
        }
        return true;
    }

    void solve() {
        checks = 0;
        System.out.println("The 8 queen problem");
        System.out.println("===================");
        if (solve(0)) {
            printBoard();
            System.out.printf("This solution took %d checks%n", checks);
        } else {
            System.out.println("There was NO solution!");
        }
    }
    boolean solve(int row) {
        // Base case; 8 queens placed?
        if (row == 8)
            return true;
        // Try every column of this row
        for(int column = 0; column < 8; column++) {
            // place queen
            board[row][column] = true;
            // Is this a valid place for this queen
            if (checkPosition(row, column)) {
                // Yes, try to solve from the next row on
                if (solve(row + 1)) {
                    // Success at last!
                    return true;
                }
            }
            // Unsuccessfully placed on this column, so retract queen and try next column
            board[row][column] = false;
        }
        // It's a dead end
        return false;
    }

    void printBoard() {
        for(int row = 0; row < 8; row++) {
            for(int column = 0; column < 8; column++) {
                System.out.print(board[row][column] ? '♕' : '-');//(row + column) % 2 == 0 ? " " : "░");
            }
            System.out.println();
        }
    }
}